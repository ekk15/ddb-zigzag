# NOTE: This was written with ChatGPT and needs to be thoroughly checked to determine correctness.

from typing import Any, Generator, Iterable, Tuple
from dataclasses import dataclass
from contextlib import closing
from functools import cached_property

from ...globals import BLOCK_SIZE
from ...profile import profile_generator
from ...validator import ValExpr, valexpr
from ...storage import BplusTree
from ..interface import QPop, ExecutorException
from .interface import JoinPop
from ..indexscan import IndexScanPop

class ZigZagJoinPop(JoinPop['ZigZagJoinPop.CompiledProps']):
    """
    Zig-zag join physical operator that uses index scans to fetch and compare keys from two sorted inputs.
    Efficiently navigates between these inputs, joining tuples based on close or exact key matches.
    """

    @dataclass
    class CompiledProps(QPop.CompiledProps):
        pass

    def __init__(self, context, left_table, right_table, left_key, right_key):
        pass

    @cached_property
    def compiled(self) -> 'CompiledProps':
        pass

    def compare(self, this: tuple, that: tuple) -> int:
        pass

    def estimated(self) -> QPop.EstimatedProps:
        pass

    @profile_generator()
    def execute(self) -> Generator[Tuple[Any, ...], None, None]:
        return NotImplementedError